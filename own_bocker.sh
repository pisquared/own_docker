# FROM https://github.com/p8952/bocker/blob/master/bocker

# setup brtfs file
fallocate -l 1G ~/btrfs.img

# create a brtfs filesystem and mount it
mkdir /var/bocker
mkfs.btrfs ~/btrfs.img
mount -o loop ~/btrfs.img /var/bocker

# Enable IP forwarding
echo 1 > /proc/sys/net/ipv4/ip_forward
iptables --flush
iptables -t nat -A POSTROUTING -o bridge0 -j MASQUERADE
iptables -t nat -A POSTROUTING -o enp0s3 -j MASQUERADE

# create network bridge called bridge0 and an IP of 10.0.0.1/24
ip link add bridge0 type bridge
ip addr add 10.0.0.1/24 dev bridge0
ip link set bridge0 up

#########################################################
# PULLING IMAGE FROM DOCKER - NOT WORKING:
image="ubuntu"
tag="bionic"

token="$(curl -sL -o /dev/null -D- -H 'X-Docker-Token: true' "https://index.docker.io/v1/repositories/$image/images" | tr -d '\r' | awk -F ': *' '$1 == "X-Docker-Token" { print $2 }')"
registry='https://registry-1.docker.io/v1'
id=$(curl -sL -H "Authorization: Token $token" "$registry/repositories/$image/tags/$tag" | sed 's/"//g')

#########################################################

btrfs_path='/var/bocker'


##########################################################
# FROM https://ericchiang.github.io/post/containers-from-scratch/

wget http://cdimage.ubuntu.com/ubuntu-base/releases/18.04/release/ubuntu-base-18.04-base-amd64.tar.gz
mkdir rootfs
sudo tar xfz ubuntu-base-18.04-base-amd64.tar.gz -C rootfs/

# 1. Simple CHROOT
# sudo chroot rootfs /bin/bash

# 2. unshare - disassociate parts of the process execution context
# unshare expects the new mountpoint to be already a mountpoint
sudo mount -t proc proc $PWD/rootfs/proc

sudo unshare -p -f --mount-proc=$PWD/rootfs/proc \
     chroot rootfs /bin/bash

# unmount proc
sudo umount $PWD/rootfs/proc

# 3. List namespaces of the container ps aux | grep /bin/bash | grep root -> find PID
sudo ls -l /proc/$PID/ns

# Enter the existing pid namespace
sudo nsenter --pid=/proc/4178/ns/pid \
  unshare -f --mount-proc=$PWD/rootfs/proc \
  chroot rootfs /bin/bash 

# 4. Mounting
mkdir readonlyfiles
echo "hello" > readonlyfiles/hi.txt
sudo mkdir -p rootfs/var/readonlyfiles
sudo mount --bind -o ro $PWD/readonlyfiles $PWD/rootfs/var/readonlyfiles

# unmount:
sudo umount $PWD/rootfs/var/readonlyfiles

# 5. create cgroup
mkdir /sys/fs/cgroup/memory/demo

# Limit the cgroup to 100MB of memory and turn off swap.
echo "100000000" > /sys/fs/cgroup/memory/demo/memory.limit_in_bytes
echo "0" > /sys/fs/cgroup/memory/demo/memory.swappiness

# join this process to the cgroup by writing to the tasks file
echo $$ > /sys/fs/cgroup/memory/demo/tasks

# exit the current process and remove the dir
exit 
sudo rmdir /sys/fs/cgroup/memory/demo

# 6. capabilities
# capabilities are only granted to binaries
# to grant CAP_NET_BIND_SERVICE (listen to priviliged ports)
sudo setcap cap_net_bind_service=+ep binary

# obtain the resolved path with `readlink -f $(which nc)`
# e.g.:
# sudo setcap cap_net_bind_service=+ep /bin/nc.openbsd
# nc -l 80
# --- works
# sudo setcap cap_net_bind_service=-ep /bin/nc.openbsd
# nc -l 80
# nc: Permission denied


##########################################################
# Continuing https://github.com/p8952/bocker/blob/master/bocker

# cgcreate, cgexec -> apt install cgroup-tools 


















